# Bronhouderkoppelvlak van de Landelijke Voorziening Bekendmaken en Beschikbaarstellen 

Dit is de repository van het bronhouderkoppelvlak van de LVBB.
In de master branch staan geen bestanden.

- Zie het [documentatie overzicht](https://koop.gitlab.io/lvbb/bronhouderkoppelvlak/index.html) voor een overzicht van de beschikbare versies van de koppelvlakspecificaties.

- De bestanden behorend bij een versie van de specificaties zijn te vinden via de *tag* met het versie als naam. Om de bestanden op te halen:
    - Ga naar de [Gitlab website](https://gitlab.com/koop/lvbb/bronhouderkoppelvlak/), selecteer linksboven het versienummer (in plaats van *master*) en download (rechtsboven) de bestanden.
    - Via een git client. Kloon deze repository en bekijk de lijst met tags. Gebruik de git client om een kloon op basis van de tag te maken.






